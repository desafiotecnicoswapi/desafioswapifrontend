import React, { Component, Fragment } from "react";
import { Divider } from '@material-ui/core';
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import "react-placeholder/lib/reactPlaceholder.css";
import { withStyles } from '@material-ui/core/styles';
const styles = {
    card: {
      minWidth: 300,
    }
  };
  export default  withStyles (styles) (class Planeta extends Component {
    constructor() {
        super();
        this.state = { ready: false }
    }

    render() {
        const { classes } = this.props;
        return (
            <Card className={classes.card}>
                <CardContent>
                    <Typography color="textPrimary" variant="title" gutterBottom>
                        {this.props.planetName}
                    </Typography>

                    <Divider variant="fullWidth" />
                    <Typography align='left'>População: {this.props.population}</Typography>
                    <Typography align='left'>Clima: {this.props.climate}</Typography>
                    <Typography align='left'>Bioma: {this.props.terrain}</Typography>
                    <Typography align='center'> Apareceu em {this.props.films} filme{this.props.films > 1 ? 's' : ''}</Typography>
                </CardContent>
            </Card>

        );
    }
})
