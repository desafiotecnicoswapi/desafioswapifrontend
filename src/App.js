import React from 'react';
import './App.css';
import Planetas from './Planetas';
import CssBaseline from '@material-ui/core/CssBaseline';

class App extends React.Component {
  constructor(props, context) {
    super(props, context);

  }

  render() {
    return (
      <div className="App" >
            <CssBaseline />
            <Planetas />
      </div>
    );
  }
}

export default App;
