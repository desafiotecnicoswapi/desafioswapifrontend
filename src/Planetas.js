import React, { Component, Fragment } from "react";
import Axios from "axios";
import Planeta from './Planeta';
import { Button } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import StarfieldAnimation from 'react-starfield-animation'
const apiUrl = "https://swapi.co/api/";
export default class Planetas extends Component {
    constructor() {
        super();
        this.state = {
            statePlaneta: 1,
            planetName: "Carregando...",
            population: 0,
            climate: "Carregando...",
            terrain: "Carregando...",
            films: 0
        };
        this.mudarPlaneta = this.mudarPlaneta.bind(this);
    }
    async componentDidMount() {
        const response = await Axios.get(apiUrl + "planets/" + this.state.statePlaneta);
        const planeta = response.data;
        console.log(planeta)
        var nFilmes = [];
        nFilmes = planeta.films;

        this.setState({
            planetName: planeta.name,
            population: planeta.population,
            climate: planeta.climate,
            terrain: planeta.terrain,
            films: nFilmes.length
        });
    }
    async carregarDados() {
        const response = await Axios.get(apiUrl + "planets/" + this.state.statePlaneta);
        const planeta = response.data;
        console.log(planeta)
        var nFilmes = [];
        nFilmes = planeta.films;

        this.setState({
            planetName: planeta.name,
            population: planeta.population,
            climate: planeta.climate,
            terrain: planeta.terrain,
            films: nFilmes.length
        });
    }
    mudarPlaneta() {
        var planeta = Math.floor(Math.random() * 61) + 1;
        var loadingState = {
            statePlaneta: planeta,
            planetName: "Carregando...",
            population: 0,
            climate: "Carregando...",
            terrain: "Carregando...",
            films: 0
        };
        this.setState(loadingState, () => {
            this.carregarDados();
        })
    }
    render() {
        const { planetName,
            population,
            climate,
            terrain,
            films } = this.state;
        return (
            <Grid container alignItems='center' justify="center" direction="column" spacing={16}>
                <StarfieldAnimation
                    style={{
                        position: 'absolute',
                        width: '100%',
                        height: '100%',
                    }} />
                <Grid item >
                    <Planeta planetName={planetName}
                        population={population}
                        climate={climate}
                        terrain={terrain}
                        films={films}></Planeta>
                </Grid>
                <Grid item>
                    <Button variant="contained"
                        color="primary"
                        onClick={this.mudarPlaneta}>
                        Próximo
                </Button>
                </Grid>
            </Grid>
        );
    }
}
